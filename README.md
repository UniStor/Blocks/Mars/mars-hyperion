# plugin: Mars-Hyperion Driver
Mars in Hyperion enables Indestructible VMs!
- replicate your active vols/cache to 2 or more sites
- while your cold data gets erasure coded & dispersed globally


# Architecture
## Storage Domain
- Hypervisor runs a Mars storage Domain!
- It contains a custom minimal kernel which's sole responsibility is to run Mars. This Domain handles all responsibilities for provisioning & replicating storage.
- DomainU's use nbd & iscsi !


# Roadmap
## from Vm, to: Driver Domain
Right now it is a complete DomainU (or VM in Kvm)
but in the future we want to simplify it to d Xen 'Driver Domain', or just an embedded service in Kvm or Container hosts.

https://wiki.xenproject.org/wiki/Driver_Domain
https://wiki.xenproject.org/wiki/Dom0


# Failover
## Scenario: hosting with a small-scale inexpensive provider
IF a small unicloud operator fails, or has a power outage? Hyperion will automatically recreate and sync your Node to a new host.

## Singularity
You simply wait for Singularity Viewer tor reconnect, and resume your work.

Option: input & event Queue
- Singularity can also capture User input, and other system events to a queue- letting it Replay any lost work.

# Alt: DRBD Driver
Also try 'DRBD Hyperion Driver' For more consistency-availability real-time guaranteers
